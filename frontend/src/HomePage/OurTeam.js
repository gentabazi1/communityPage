import React from 'react';
import Anetari1 from './Styling/Images/anetari1.png';
import Anetari2 from './Styling/Images/anetari2.png';
import Anetari3 from './Styling/Images/anetari3.png';
import Anetari4 from './Styling/Images/anetari4.png';

class OurTeam extends React.Component{
    render() {
      return (
        <div>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <style dangerouslySetInnerHTML={{__html: "\nhtml {\n  box-sizing: border-box;\n}\n\n*, *:before, *:after {\n  box-sizing: inherit;\n}\n\n.column {\n  float: left;\n  width: 20%;\n  margin-bottom: 16px;\n  padding: 0 8px;\n}\n\n@media screen and (max-width: 650px) {\n  .column {\n    width: 100%;\n    display: block;\n  }\n}\n\n.card {\n  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\n}\n\n.container {\n  padding: 0 16px;\n}\n\n.container::after, .row::after {\n  content: \"\";\n  clear: both;\n  display: table;\n}\n\n.title {\n  color: grey;\n}\n\n.button {\n  border: none;\n  outline: 0;\n  display: inline-block;\n  padding: 8px;\n  color: white;\n  background-color: #000;\n  text-align: center;\n  cursor: pointer;\n  width: 100%;\n}\n\n.button:hover {\n  background-color: #555;\n}\n" }} />
          <div className="row">
            <div className="column">
              <div className="card">
                <img src={Anetari1} alt="CEO" style={{width: '100%'}} />
                <div className="container">
                  <h2>Jane Doe</h2>
                  <p className="title">CEO &amp; Founder</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  
                </div>
                
                
              </div>
              
            </div>
            <div className="column">
              <div className="card">
                <img src={Anetari2} alt="Worker" style={{width: '100%'}} />
                <div className="container">
                  <h2>Worker</h2>
                  <p className="title">Art Director</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  
                </div>
              </div>
            </div>
            <div className="column">
              <div className="card">
                <img src={Anetari3} alt="Worker" style={{width: '100%'}} />
                <div className="container">
                  <h2> Worker</h2>
                  <p className="title">Designer</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  
                </div>
              </div>
            </div>
            <div className="column">
              <div className="card">
                <img src={Anetari4} alt="CEO" style={{width: '100%'}} />
                <div className="container">
                  <h2>Jane Doe</h2>
                  <p className="title">CEO &amp; Founder</p>
                  <p>Some text that describes me lorem ipsum ipsum lorem.</p>
                  <p>example@example.com</p>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  };
export default OurTeam;