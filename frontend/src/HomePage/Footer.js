import React from 'react';
import RFLOGO from './Styling/Images/RFLOGO.png';
import Button from '@material-ui/core/Button';
import CenteredGrid from '../grid';



class Footer extends React.Component{
 

  render (){
    return (
      <div>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
        <style dangerouslySetInnerHTML={{__html: "\n            footer {\n                background-color: #bababa;\n                clip-path: polygon(49% 12%, 90% 19%, 100% 21%, 100% 100%, 0 100%, 0 60%, 0 0);\n            }\n            #h1{\n                text-align: center;\n            }\n            #copyright{\n                text-align: center;\n                font-family: Lusitana;\n                font-size: 18px;\n            }\n            #abc{\n                text-align: center;\n            }\n            #contact{\n                font-size: 20px;\n                font-family: Montserrat;\n            }\n            #about{\n                font-family: Lusitana;\n                font-size: 22px;\n            }\n            #btn{\n                background-color: #F1E813;\n                font-size: 20px;\n            }\n            #nav{\n                -webkit-text-fill-color: black;\n            }\n            #nav:hover{\n                background-color: gray;\n            }\n            #nav2{\n                margin-left: 50px;\n            }\n            #bank{\n                height: 65px;\n                width: 150px;\n            }\n            #about2{\n                text-align: center;\n            }\n            #p{\n                font-size: 23px;\n            }\n            #top{\n                margin-top: 50px;\n            }\n        " }} />
        <footer>
          <div className=" footer-top">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-4 col-lg-4 " id="top">
                  <br />
                  <p id="contact"> Raiffeisen Bank Kosovo J.S.C. </p>
                  <p id="contact"> Zyra qendrore Adresa:</p>
                  <p id="contact"> Rruga UÇK, nr. 191 10000 Prishtinë, Kosovë</p>
                  <img className="img-fluid" src={RFLOGO} />
                </div>
                <div className="col-md-5 col-lg-5 d-flex align-items-end" style={{marginTop:100}} >
                  <div className='row '>
                    <div className='col-md-2 col-lg-2 text-center'><Button>Home</Button></div>
                    <div className='col-md-4 col-lg-4 text-right '><Button>About Us</Button></div>
                    <div className='col-md-3 col-lg-3 text-center'><Button>Community</Button></div>
                    <div className='col-md-3 col-lg-3 text-center'><Button>Blogs</Button></div>
                  </div>
                 
                </div>
                <div className="col-md-3 col-lg-3 footer-links wow fadeInUp">
                  
                    
                    <br />
                      <br />
                      <br />
                      <br />
                      <br />
                      <p id="about">Want to know more about us ?</p>
                      <button type="button" className="btn" id="btn">Sign Up</button>
                    
                  
                </div>
                
                
              </div>
              <div style={{marginTop:10}} className='text-center'><h6>© 2019 Raiffeisen Bank Kosovo</h6></div>
            </div>
          </div>
          </footer>
          
      </div>
  
  );
    }
    }
  
    

export default Footer;
