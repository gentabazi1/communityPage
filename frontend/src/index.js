import React , {Component} from 'react';
import ReactDOM from 'react-dom';

import Footer from './HomePage/Footer';

import OurTeam from './HomePage/OurTeam';
import CenteredGrid from './HomePage/demo';
import AllBlogs from './AllBlog';
import MainHeader from './Header';
import {BrowserRouter , Route, Switch } from 'react-router-dom';
import Error from './Error';



    




class App extends React.Component{
    render(){
    return (
        
       
        <BrowserRouter >
        <div>
            <MainHeader/>
        <Switch>
            <Route path={"/blogs"} component={AllBlogs}/>
            <Route path={"/"} component={CenteredGrid} exact />
            <Route component={Error}/>
            </Switch>
            <Footer/>
        </div>
        </BrowserRouter>
      

        /*<div>
        <MainHeader/>

       <CenteredGrid/>
        {/*<AllBlog />
       
        <Footer></Footer>
        
        
        </div>
*/
    )
    
}



};
ReactDOM.render(<App />, document.getElementById('root'));
