import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ContainedButtons from './buttons';
import ContainedButtons1 from './buttons.1';
import ContainedButtons2 from './buttons.2';
import ContainedButtons3 from './buttons.3';

import Button from '@material-ui/core/Button';



const styles = theme => ({
btn:{
    width:'100%',
},
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});



function CenteredGrid(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
    
      <Grid container spacing={0}>
        <Grid item xs={3}>
        <Button variant="contained" color="primary" className={classes.button}>
        Home
      </Button>
      </Grid>
  
      <Grid item xs={4}>
      <Button variant="contained" color="primary" className={classes.button}>
        About Us
      </Button>
      </Grid>
     
       <Grid item xs={3}>
      <Button variant="contained" color="primary" className={classes.button}>
        Community
      </Button>
      </Grid>
      
      <Grid item xs={3}>
      <Button variant="contained" color="primary" className={classes.button}>
        Blog
      </Button>
      </Grid>
    
        </Grid>
        
       
     
      
    </div>
  );
}

CenteredGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);
