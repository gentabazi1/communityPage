from django.contrib import admin

from .models import Blogs, Kategorite, Challenges, Comments_challenges, Comments_blogs 

admin.site.register(Blogs)
admin.site.register(Kategorite)
admin.site.register(Challenges)
admin.site.register(Comments_challenges)
admin.site.register(Comments_blogs)